/* eslint no-underscore-dangle: ["error", { "allow": ["_data", "_queriedProps", "_depTable", "_currCBname", "_currCallBack", "_funsAndNames"] }] */

function createSmartObject(obj) {
  const keys = Object.keys(obj);
  const newObj = Object.assign({}, obj);
  newObj._data = { ...obj };
  newObj._queriedProps = new Set();
  newObj._depTable = {};

  keys.forEach(key => {
    newObj._depTable[key] = new Set();
    Object.defineProperty(newObj, key, {
      get() {
        this._queriedProps.add(key);
        return this._data[key];
      },
      set(val) {
        if (val !== undefined && this.key !== val) {
          this._data[key] = val;
          const depFns = [];
          Object.keys(this._depTable).forEach(propName => {
            if (propName === key && this._depTable[propName].size !== 0) {
              depFns.push(...this._depTable[propName]);
            }
          });
          depFns.forEach(fn => {
            fn();
            const cb = [...this._depTable[key]].shift();
            if (cb === fn) this._depTable[key].delete(fn);
          });
        }
      },
      enumerable: false,
      configurable: false,
    });
  });
  return newObj;
}

const oldObj = {
	surname: 'Ivanov',
	name: 'Vasya',
	patronymic: 'Olegovich',
};
const obj = createSmartObject(oldObj);

function defineComputedField(obj, computedProp, fn) {
  const wrapFn = () => {
    obj._queriedProps.clear();
    obj[`_${computedProp}`] = fn(obj);
    obj._queriedProps.forEach(prop => obj._depTable[prop].add(wrapFn));
  };
  Object.defineProperty(obj, computedProp, {
    get() {
      return this[`_${computedProp}`];
    },
    set() {
      throw new Error('Cannot assign a new value to computed property');
    },
    enumerable: false,
    configurable: false,
  });

  wrapFn();
}

defineComputedField(obj, 'fullName', function(data) {
	return data.name + ' ' + data.surname + ' ' + data.patronymic;
});
console.log(obj.fullName); // Vasya Ivanov Olegovich

defineComputedField(obj, 'firstLastName', data => {
	return data.name + ' ' + data.surname;
});

console.log(obj.fullName); // Vasya Ivanov Olegovich
obj.surname = 'Changed';
console.log(obj.surname);
console.log(obj.firstLastName); // Vasya Changed Olegovich
// obj.fullName = 'foo' // error
