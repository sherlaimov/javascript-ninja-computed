# computed

Что такое "**computed**" поля - это поля, которые "магическим образом" рассчитываются на основе других.
Представим что у нас есть поле "Имя", поле "Фамилия" и поле "Отчество" - на основании их мы можем магически рассчитать поле "ФИО".
Ваша задача - реализовать это. Ваша реализация будет состоять из двух функций - **createSmartObject** - создающих объект с "умной"
функциональностью" и __defineComputedField__, которая добавляет в объект умное поле.
Внимание: "умное" поле должно обновлять свое значение тогда и только тогда, когда изменяют значения поля, от которых оно зависит. 
Как выглядит код:

### Уровень сложности 1:
```javascript
const obj = createSmartObject({
  name: 'Vasya',
  surname: 'Ivanov',
  patronimic: 'Olegovich',
});

defineComputedField(obj, 'fullName', ['name', 'surname', 'patronimic'], (name, surname, patronimic) => {
  return name + ' ' + surname + ' ' + patronimic;
});

console.log(obj.fullName); // Vasya Ivanov Olegovich
obj.surname = 'Petrov';
console.log(obj.fullName); // Vasya Petrov Olegovich
obj.fullName = 'foo' // error
```
### Уровень сложности 2:
```javascript
const obj = createSmartObject({
  name: 'Vasya',
  surname: 'Ivanov',
  patronimic: 'Olegovich',
});

defineComputedField(obj, 'fullName', function (data) {
  return data.name + ' ' + data.surname + ' ' + data.patronimic;
});
// Да, от каких полей зависит наш computed вычисляется МАГИЧЕСКИ

console.log(obj.fullName); // Vasya Ivanov Olegovich
obj.surname = 'Petrov';
console.log(obj.fullName); // Vasya Petrov Olegovich
obj.fullName = 'foo' // error
```



   [![N|Solid](https://pbs.twimg.com/profile_images/885289927402967040/TOV3ypMv.jpg)](https://javascript.ninja)
